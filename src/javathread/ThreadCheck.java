/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sujin
 */
public class ThreadCheck {

    public static void main(String[] args) throws InterruptedException {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= 10; i++) {
                    try {
                        System.out.println("t" + i);
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        System.err.println("Thread has been interupted");
                        return;
                    }
                }
            }
        });

        t.start();
        Thread.sleep(5000);
        if (t.isAlive()) {
            System.out.println("Still waiting...");
        }
        //t.interrupt();
        t.join();
        System.out.println("Thread completed");
    }

}
