
package javathread.blockingq;

import java.util.ArrayList;

public class BlockingQueue {

    private static ArrayList<Integer> arrData = new ArrayList<Integer>();
    private static int SIZE;

    public BlockingQueue(int size) {
        SIZE = size;
    }

    public synchronized void enqueue(int data) throws InterruptedException {
        if (arrData.size() > SIZE) {
            wait();
        }

        arrData.add(data);
        notify();

    }

    public synchronized int dequeue() throws InterruptedException {
        if (arrData.size() == 0) {
            wait();
        }
        int data = arrData.remove(0);
        notify();
        return data;
    }

}
