/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathread.blockingq;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sujin
 */
public class BlockingQueueDemo {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue bq = new BlockingQueue(10);
        //int arr[] ={1,2,3,4,5,6};

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    try {
                        bq.enqueue(i);
                    } catch (InterruptedException ex) {
                        return;
                    }

                }
                // notify();

            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 1000; i++) {
                        int data = bq.dequeue();
                        System.out.println("Data:" + data);
                    }

                } catch (InterruptedException ex) {
                    return;
                }
            }
        });

        t1.start();
        t2.start();

    }
}
