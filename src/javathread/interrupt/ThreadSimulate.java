/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathread.interrupt;

/**
 *
 * @author Sujin
 */
public class ThreadSimulate {

    public static void heavyDump() {
        long start = System.currentTimeMillis();
        while(System.currentTimeMillis()-start<1000);
            
        

    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    heavyDump(); //Thread.sleep(1000);
                    System.out.println("Executing thread");
                    if(Thread.interrupted()){
                        System.out.println("Thread is interrupted");
                        return;
                    }
                        
                }

            }
        });

        thread1.start();
        Thread.sleep(2000);
        thread1.interrupt();
    }

}
