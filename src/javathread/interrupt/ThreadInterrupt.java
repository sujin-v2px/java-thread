/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathread.interrupt;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sujin
 */
public class ThreadInterrupt {

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            try {
                System.out.println("Thread is started");
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                System.err.println("Thread has been interupted");
                return;
            }
            System.out.println("Thread Complete");
        });

        thread1.start();

        Thread.sleep(1000);
        thread1.interrupt();
    }

}
